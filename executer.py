#!/usr/bin/env python3
''' Main job executer '''

from definitions import base_run_dir, base_results_dir
import argparse
import os
import shutil

# Execute the job
def execute_job( release = '23.0.53',
                 nthreads = 8,
                 nevents = 2750,
                 job = 'data23-mu61p9_RAWtoALL' ):

    # Let's figure out where we are
    repo_path = os.path.dirname( os.path.realpath( __file__ ) )

    # Set the full run/results directories
    run_dir = f'{base_run_dir}/{job}-{nthreads}-{nevents}'
    results_dir = f'{base_results_dir}/{job}-{nthreads}-{nevents}'

    # Check to see if we ran the job already
    if os.path.exists(results_dir):
        raise RuntimeError( f'Results directory exists, did you already run this job?' )

    # Check to see the script exists
    job_script = f'{repo_path}/scripts/{job}.sh'
    if not os.path.exists(job_script):
        raise RuntimeError( f'Cannot find the script for job {job}, quitting...' )

    # Print some information
    print( f'Processing job {job} for {nevents} events in {nthreads} threads' )

    # Now go to the run directory, setup the release and run the job
    os.system( f'mkdir -p {run_dir}; cd {run_dir};'
               f'source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh > /dev/null 2>&1;'
               f'lsetup "asetup Athena,{release}" > /dev/null 2>&1;'
               f'. {job_script} {nthreads} {nevents};' )

    # Check to see if the job succeeded
    with open( f'{run_dir}/__exitcode' ) as f:
        for line in f:
            line = line.strip()
            if line != '0':
                raise RuntimeError( f'Job has a non-zero exit code {line}!' )

    # Now let's copy the relevant outputs to the final directory
    os.system( f'mkdir -p {results_dir}; cp {run_dir}/{{__stdout,perfmonmt*}} {results_dir}/.;' )

    # Now remove the run directory
    shutil.rmtree(run_dir)

# Define the main function
def main():

    # Setup the job parameters here...
    parser = argparse.ArgumentParser(description = 'Basic Job Executer',
                                     add_help = True)

    parser.add_argument( '-j', '--job', action = 'store',
                         dest = 'job', default = 'data23-mu61p9_RAWtoALL',
                         help = 'The job that will be executed (default: data23-mu61p9_RAWtoALL)' )
    parser.add_argument( '-r', '--release', action = 'store',
                         dest = 'release', default = '23.0.53',
                         help = 'The release that will be used (default: 23.0.53)' )
    parser.add_argument( '-t', '--threads', action = 'store',
                         dest = 'threads', default = '8',
                         help = 'Number of threads the job will use (default: 8)' )
    parser.add_argument( '-e', '--events', action = 'store',
                         dest = 'events', default = '2750',
                         help = 'Number of events the job will execute (default: 2750)' )
    parser.add_argument( '-v', '--version', action = 'version', version = '%(prog)s 0.1' )

    args = parser.parse_args()

    # Execute the job
    execute_job( release = args.release,
                 nthreads = args.threads,
                 nevents = args.events,
                 job = args.job )


# When running this as a script
if '__main__' in __name__:
    main()
