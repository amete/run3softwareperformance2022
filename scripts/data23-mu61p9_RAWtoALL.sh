#!/bin/bash

# ${1} = Number of threads
# ${2} = Number of events

touch __start;

TRF_ECHO=1 ATHENA_CORE_NUMBER="${1}" Reco_tf.py \
  --CA "True" \
  --perfmon "fullmonmt" \
  --inputBSFile "/data/amete/scratch/run3-performance-study-inputs/data23_13p6TeV/data23_13p6TeV.00454083.physics_Main.daq.RAW._lb1100._SFO-11._0001.data" \
  --maxEvents "${2}" \
  --outputAODFile "myAOD.pool.root" \
  --outputHISTFile "myHIST.root" \
  --multithreaded "True" \
  --autoConfiguration "everything" \
  --conditionsTag "CONDBR2-BLKPA-2023-02" \
  --geometryVersion "ATLAS-R3S-2021-03-02-00" \
  --runNumber "454083" \
  --steering "doRAWtoALL" > __stdout 2>&1

echo $? > __exitcode;

touch __done;
